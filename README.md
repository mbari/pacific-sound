[![MBARI](https://www.mbari.org/wp-content/uploads/2014/11/logo-mbari-3b.png)](http://www.mbari.org)

# About

This repository contains calibration information about hydrophones currently deployed in the MARS network for the [Soundscape Project](https://www.mbari.org/mars-hydrophone/).
