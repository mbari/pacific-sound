# Pacific Sound Changelog

# 1.0.0 (2021-10-11)


### Features

* initial check-in ([7e08da9](http://bitbucket.org/mbari/pacific-sound/commits/7e08da9cd060b3d4c58ada2f41cd4ccbad34f15c))
